# Chapter 2 notes

## Cursor methods

Applied on the server side. Can be called anytime you havent fetched results or called hasNext()

* sort *method*
    Order results by field. Can use 1 (ascending) or -1 (descending)
* limit *method*
    Limit the ammount of results returned
* skip *method*
    Don't return the ammount of results passed

## Counting results

* count
    Allows counting number of results instead of returning the actual results

## Updating documents

* update *method*
    Used like the find method. First argument finds the docs to update, the second has the data you want to insert into the documents.
    BUT, it wipes anything the doc may have, letting only the new data in the second argument on the doc and discarding the rest.

    Performs 4 operations:
    * Full document replacement
    * Partial document modification
    * Upserts (Update or insert if no documment exists matching the update criteria)
    * Update multiple documents

    * $set *command*
    Used inside the update method. You can specify the fields that you want to add or modify and they will be inserted or updated
    accordingly.

    * $inc *command*
    Used inside the update method. When you know some field is a numerical one and one it increased by any ammount you can use this
    command and the field will be updated. This field may not exist and $inc will add it with a value equal to the increment step

    * $unset *command*
    Used inside the update method. Lets you remove fields passed as argument, so you don't need a previous query to know what
    fields and values you have to keep and what to keep out

    You can access positions inside arrays using it's decimal position, so:
    {"_id": "Javi", "rates": [1, 2, 3, 4]}
    db.rates.find({"_id": "Javi"}, {$set: {"rates.3": 5}})
    Will modify the document like this:
    {"_id": "Javi", "rates": [1, 2, 3, 5]}

    * $push, $pop, $pull *command*
    Used inside the update method. *$push* is used to add an element to the array on the rightmost position and *$pop* removes one
    from the rightmost position too. With a -1 value, $pop removes the element from the leftmost position. *$pull* removes any
    occurence of a value from the array no matter it's position.

    * $addToSet *command*
    Add value to the array only if it doesn't yet exist


## Upserts

    Used to insert values when you don't even know if they exist. It's passed as a third argument to the update method.
    Notice that if the find part of the update method doesn't find any matches, the upsert will still create a new
    document with the contents available both from the search and the data args. If the search argument contains something
    like $gt or the like, then they will not be used

## Multi-update

    The empty set selects all the docs on the collection. This can be used on the *update* and *find* methods.
    The update method by default only operates over one document, the first it finds with the search query.
    To update all you have to pass as argument multi: true
    Some API implementations include a update and multiupdate methods to further separate this two operations.
