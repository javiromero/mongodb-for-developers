# -*- coding: utf-8 -*-

import pymongo
import datetime

# establish a connection to the database
connection = pymongo.MongoClient("mongodb://localhost")


def add_review_date_using_update_one(student_id):
    """add a review date to a single record using update_one"""

    print("updating record using update_one and $set")
    # get a handle to the school database
    db = connection.school
    scores = db.scores

    try:
        # get the doc
        score = scores.find_one({
            'student_id': student_id,
            'type': 'homework'
        })
        print("before: ", score)

        # update using set
        record_id = score['_id']
        now = datetime.datetime.utcnow()
        result = scores.update_one({'_id': record_id},
                                   {'$set': {'review_date': now}})
        print("num matched: ", result.matched_count)

        score = scores.find_one({'_id': record_id})
        print("after: ", score)
    except Exception as e:
        raise


def add_review_dates_for_all():
    """add a review date to all records"""

    print("updating record using update_one and $set")
    # get a handle to the school database
    db = connection.school
    scores = db.scores

    try:
        # update all the docs
        now = datetime.datetime.utcnow()
        result = score = scores.update_many({},
                                            {'$set': {'review_date': now}})
        print("num matched: ", result.matched_count)
    except Exception as e:
        raise


if __name__ == '__main__':
    # add_review_date_using_update_one(1)
    add_review_dates_for_all()
