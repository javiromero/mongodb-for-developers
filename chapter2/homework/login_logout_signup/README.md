# Attention

This chapter has a homework (2.3) that requires you to run a script designed to
work only on python2, so you must create a virtualenv with python2 as
interpreter in order to be able to run the script.

You can find the python 3 compatible script on validate.py3k
