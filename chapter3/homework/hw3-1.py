# -*- coding: utf-8 -*-

"""Homework 3.1, remove lowest homework score on every student"""

import pymongo


def main():
    connection = pymongo.MongoClient("mongodb://localhost")
    db = connection.school
    students = db.students

    query = {
        'scores.type': "homework",
    }
    try:
        cursor = students.find(query)
    except Exception as e:
        print("Unexpected error:", type(e), e)

    for doc in cursor:
        """
        Pretty sure we can make this using $push, $sort and $slice somehow
        """
        # print("doc", doc)
        min_score = 100
        student_scores = doc['scores']
        # print("student_scores", student_scores)
        for each in student_scores:
            if (each['type'] == 'homework' and each['score'] < min_score):
                min_score = each['score']

        # print("min_score", min_score)
        updated_scores = [
            {'type': x['type'], 'score': x['score']} for x in student_scores
                if (x['type'] != 'homework' or
                    (x['type'] == 'homework' and
                     x['score'] != min_score))]
        # print("updated_scores", updated_scores)
        students.update_one({'_id': doc['_id']},
                            {'$set': {'scores': updated_scores}})


if __name__ == '__main__':
    main()
