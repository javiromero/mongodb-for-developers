# Chapter 3 notes

The most important thing to note here is that the schema design always tries
to match the data access patterns of the application, because that's the way
you are going to get the best performance and the most convinient programming
of the application

## What about the lack foreign key constrains?

There's none. You and your app are the only responsibles to keep data consistent
and the only ones to blame if anything fails :)

## And what about the lack of transactions?

None as well. But! Mongodb has atomic operations :D.
In a SQL server you may do operations affecting several tables, so you need
transactions to keep things consistent. In mongodb you already join all the
data into the documents and the documents are guaranteed to be inserted or
updated in an atomic operation.

Also, they are actually advising to tolerate inconsistencies O_O

## Relations

### One to One:

    Depending on the frequency of access, you may want to embed it or separate
    the docs on different collections and link by their _id.
    Also if the document size is too large (more than 16MB) or if there's one
    doc that may exist while the other may take some time to be added, or if
    you are only updating one part of the relation instead of both you may want
    to separate the docs on its own collections.

### One to Many:

    Study if this is actually a one to many or if the relation is actually just
    one to few. It it's one to few just embed it.
    If it's actually one to many, separate collections and use linking by using
    in the :1 document a value for the field and in the :M document using that
    value as the document _id

### Many to Many:

    Same here, check if the data is actually related many to many or few to few.
    You can very well use linking adding in one document the _ids of the other,
    or you can embed them

    As with everything relation in mongodb, be really warned about data
    consistency, as there's no guarantee that anything you insert as linking
    is acually there :(


There's a pattern here right? In almost every case there's the recommendation
to embed the docs inside other docs for relations. This has performance impact
as we can use locallity to our benefit with data closer in the memory storage.

## Trees

Just throw a buch of _ids in an ancestors array and the parent _id in a key,
so you can query them and navigate up and down

## Large documents/files

MongoDB includes GridFS that allows us to store large files. Then we use the
_id on a document in another collection
