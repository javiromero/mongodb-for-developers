# -*- coding: utf-8 -*-
"""Using forms with bottle"""

import bottle


@bottle.route('/')
def home_page():
    """Home page displaying a list of stuff and a form input"""

    mythings = ['apple', 'orange', 'banana', 'peach']
    return bottle.template('hello_world', {
        'username': 'Victor',
        'things': mythings,
    })


@bottle.post('/favorite_fruit')
def favorite_fruit():
    """Page handling the input sent on the form"""

    fruit = bottle.request.forms.get('fruit')
    if not fruit:
        fruit = 'No fruit selected'

    bottle.response.set_cookie("fruit", fruit)
    bottle.redirect('/show_fruit')


@bottle.route('/show_fruit')
def show_fruit():
    """Get the favorite fruit from the cookies and show it"""

    fruit = bottle.request.get_cookie('fruit')
    return bottle.template('fruit_selection.tpl', {
        'fruit': fruit,
    })


bottle.debug(True)
bottle.run(host='0.0.0.0', port=8082)
