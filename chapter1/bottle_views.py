# -*- coding: utf-8 -*-
"""Starting with bottle views"""

import bottle


@bottle.route('/')
def home_page():
    """A homepage displaying a list of stuff"""
    mythings = ['apple', 'orange', 'banana', 'peach']
    return bottle.template('hello_world', username='Javi', things=mythings)
    # return bottle.template('hello_world', {
    #     'username': 'Victor',
    #     'things': mythings,
    # })


bottle.debug(True)
bottle.run(host='0.0.0.0', port=8082)
