# -*- coding: utf-8 -*-
"""
Introduction to pymongo usage for Mongodb for developers
"""

from pymongo import MongoClient


def print_name():
    """ Connects to the test database and gets the firts name"""
    # connect to database
    connection = MongoClient('localhost', 27017)

    db = connection.test

    # handle to names connection
    names = db.names

    item = names.find_one()

    print(item['name'])


if __name__ == '__main__':
    print_name()
