<!DOCTYPE html>
<html>
<head><title>Hello World!</title></head>
<body>
<p>Welcome {{username}}</p>
<p>
    <ul>
    %for thing in things:
        <li>{{thing}}</li>
    %end
    </ul>
</p>
<form action="/favorite_fruit" method="POST">
    <label>What is your favorite fruit?</label>
    <input type="text" name="fruit" value="">
    <input type="submit" value="Submit">
</form>
</body>
</html>
