# -*- coding: utf-8 -*-
"""
Bottle tutorial
"""

import bottle


@bottle.route('/')
def home_page():
    """Return a simple home"""

    return "<html><title></title><body>Hello World!</body></html>\n"


@bottle.route('/testpage')
def test_page():
    """Return a simple page for testing routes"""

    return "This is a test page"


bottle.debug(True)
bottle.run(host='0.0.0.0', port=8082)
