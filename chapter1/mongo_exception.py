# -*- coding: utf-8 -*-
"""Using exceptions with MongoDB and pymongo"""

import pymongo


def main():
    """Writes two times the same doc into the collection"""
    connection = pymongo.MongoClient("mongodb://localhost")
    db = connection.test
    users = db.users

    doc = {'firstname': 'Javi', 'lastname': 'Romero'}
    print(doc)
    print("About to insert the document...")

    try:
        users.insert_one(doc)
    except Exception as e:
        print("insert failed:", e)

    # Notice the _id key in the returned value
    print(doc)

    # In order to insert the same values again, we have to send a dict
    # missing the already existing _id
    doc = {'firstname': 'Javi', 'lastname': 'Romero'}
    print("Inserting again...")

    try:
        users.insert_one(doc)
    except Exception as e:
        print("second insert failed:", e)

if __name__ == "__main__":
    main()
