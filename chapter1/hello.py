#! -*- coding: utf-8 -*-
"""
Hello world on a Web Server
"""

import bottle
import pymongo

# This is the handler for the default path of the web server


@bottle.route('/')
def index():
    """Hello guys"""

    connection = pymongo.MongoClient('localhost', 27017)
    db = connection.test
    name = db.names
    item = name.find_one()

    return '<b>Hello %s!</b>' % item['name']


bottle.run(host='0.0.0.0', port=8082)
