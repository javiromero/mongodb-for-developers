#!/usr/bin/env bash
# Create a virtualenv on the system an install the required packages
source $HOME/.venvburrito/startup.sh
mkvirtualenv mongodb-for-developers -p /usr/bin/python3
cd /vagrant/
pip install -r requirements.txt
