Chapter 4
*********

Homework 4.3
============

First, we create the index on the blog post date with descending order:


.. code-block:: javascript

    use blog
    db.posts.createIndex({'date': -1})

Then, create an index for the permalinks

.. code-block:: javascript

    db.posts.createIndex({'permalink': 1})

Now, for the tags we need a complex index

.. code-block:: javascript

    db.posts.createindex({'tags': 1, 'date': -1})


Homework 4.4
============

We need to query for the database school2 and the students collection.
Then sort on millis and get the greater one:

.. code-block:: javascript

    use m101
    db.profile.find({'ns': 'school2.students'}).sort({'millis': -1}).limit(1).pretty()
